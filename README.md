# 整合activiti7和springboot

## 简介

本项目使用activiti7版本与springboot整合，activiti7提供了两个新的api（`TaskRuntime`和`ProcessRuntime`）使得activiti7的使用更加简便,但是它的鉴权默认使用了`spring security`,反面来说也是更加的不灵活。

## 核心依赖

```xml
<!-- springboot 依赖 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
<!-- mybatis 依赖 -->
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>2.1.4</version>
</dependency>
<!-- activiti 依赖 -->
<dependency>
    <groupId>org.activiti</groupId>
    <artifactId>activiti-spring-boot-starter</artifactId>
    <version>7.1.0.M4</version>
</dependency>
<dependency>
    <groupId>org.activiti.dependencies</groupId>
    <artifactId>activiti-dependencies</artifactId>
    <version>7.1.0.M4</version>
    <type>pom</type>
</dependency>
```

## 修复SQL

> 配置成功后，启动项目，会自动在数据库中创建好相应的表，但是这些表缺少了几个字段，需要修复下,这是官方的BUG。

```sql
-- ----------------------------
-- 修复Activiti7的M4版本缺失字段Bug
-- ----------------------------
alter table ACT_RE_DEPLOYMENT add column PROJECT_RELEASE_VERSION_ varchar(255) DEFAULT NULL;
alter table ACT_RE_DEPLOYMENT add column VERSION_ varchar(255) DEFAULT NULL;
```

## 关于`TaskRuntime` 和`ProcessRuntime`

> 这两个`Service`对`Activiti`原有提供的`Service`做了二次封装，而且必须是拥有`ACTIVITI_USER`角色才能调用这些`API`

```java
@PreAuthorize("hasRole('ACTIVITI_USER')")
public class ProcessRuntimeImpl implements ProcessRuntime {
	...
}

@PreAuthorize("hasRole('ACTIVITI_USER')")
public class TaskRuntimeImpl implements TaskRuntime {
 	...   
}
```

## 结尾

该项目只是一个半成品，对`Activiti`的功能浅尝辄止，最开始的初衷是想通过在BPMN流程图上配置相关属性，后端自动生成相应的业务流表单的，但是要实现还是有些困难的，时间原因就没有继续深入下去。