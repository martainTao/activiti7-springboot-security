package club.martaintao.activiti7springbootdemo;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;

import java.util.List;

public class TaskTest {

    public void TaskTest(){
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = processEngine.getTaskService();

        TaskQuery taskQuery = taskService.createTaskQuery();
        Task task = taskQuery.processInstanceBusinessKey("businessKey").singleResult();
        taskService.claim(task.getId(),"userId");
        List<Task> tasks = taskService.createTaskQuery().taskAssignee("").list();
    }

}
