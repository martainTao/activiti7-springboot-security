package club.martaintao.activiti7springbootdemo;

import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.history.*;

import java.util.List;

/**
 * @Author haitao
 * @Date 2021-01-21
 **/
public class HistoryTest {

    public void test(){
        String instanceId = "";
        String processDefinitionId = "";
        String taskId="";
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        HistoryService historyService = processEngine.getHistoryService();
        List<HistoricActivityInstance> list = historyService.createHistoricActivityInstanceQuery().processInstanceId("")
                .orderByHistoricActivityInstanceStartTime()
                .asc().list();

        /**
         * 历史详情查询
         */
        HistoricDetailQuery historicDetailQuery = historyService.createHistoricDetailQuery();
        List<HistoricDetail> historicDetails = historicDetailQuery.processInstanceId(instanceId).orderByTime().list();
        for (HistoricDetail hd: historicDetails) {
            System.out.println("流程实例ID:"+hd.getProcessInstanceId());
            System.out.println("活动实例ID:"+hd.getActivityInstanceId());
            System.out.println("执行ID:"+hd.getTaskId());
            System.out.println("记录时间:"+hd.getTime());
        }


        /**
         * 历史流程实例查询
         */
        HistoricProcessInstanceQuery historicProcessInstanceQuery = historyService.createHistoricProcessInstanceQuery();
        List<HistoricProcessInstance> processInstances = historicProcessInstanceQuery.processDefinitionId(processDefinitionId).list();
        for (HistoricProcessInstance hpi : processInstances) {
            System.out.println("业务ID:"+hpi.getBusinessKey());
            System.out.println("流程定义ID:"+hpi.getProcessDefinitionId());
            System.out.println("流程定义Key:"+hpi.getProcessDefinitionKey());
            System.out.println("流程定义名称:"+hpi.getProcessDefinitionName());
            System.out.println("流程定义版本:"+hpi.getProcessDefinitionVersion());
            System.out.println("流程部署ID:"+hpi.getDeploymentId());
            System.out.println("开始时间:"+hpi.getStartTime());
            System.out.println("结束时间:"+hpi.getEndTime());
        }

        /**
         * 历史任务实例查询
         */
        HistoricTaskInstanceQuery historicTaskInstanceQuery = historyService.createHistoricTaskInstanceQuery();
        List<HistoricTaskInstance> taskInstances = historicTaskInstanceQuery.taskId(taskId).list();
        for (HistoricTaskInstance hti : taskInstances) {
            System.out.println("开始时间:"+hti.getStartTime());
            System.out.println("结束时间:"+hti.getEndTime());
            System.out.println("任务拾取时间:"+hti.getClaimTime());
            System.out.println("删除原因:"+hti.getDeleteReason());
        }

        HistoricActivityInstanceQuery historicActivityInstanceQuery = historyService.createHistoricActivityInstanceQuery();
        List<HistoricActivityInstance> historicActivityInstances = historicActivityInstanceQuery.processInstanceId(instanceId).list();
        for (HistoricActivityInstance hai : historicActivityInstances) {
            System.out.println("活动ID:"+hai.getActivityId());
            System.out.println("活动类型:"+hai.getActivityType());
            System.out.println("活动名称:"+hai.getActivityName());
            System.out.println("任务ID:"+hai.getTaskId());
        }

        /**
         * 变量历史信息查询
         */
        HistoricVariableInstanceQuery historicVariableInstanceQuery = historyService.createHistoricVariableInstanceQuery();
        List<HistoricVariableInstance> variableInstances = historicVariableInstanceQuery.processInstanceId(instanceId).list();
        for (HistoricVariableInstance hva :
                variableInstances) {
            System.out.println("变量名称："+hva.getVariableName());
            System.out.println("变量类型名称："+hva.getVariableTypeName());
            System.out.println("变量值："+hva.getValue());
            System.out.println("流程实例ID："+hva.getProcessInstanceId());
            System.out.println("任务ID："+hva.getTaskId());
        }
    }
}
