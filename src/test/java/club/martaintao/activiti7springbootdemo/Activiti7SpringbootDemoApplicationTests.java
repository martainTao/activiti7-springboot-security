package club.martaintao.activiti7springbootdemo;

import club.martaintao.activiti7springbootdemo.utils.SecurityUtil;
import org.activiti.api.process.model.ProcessInstance;
import org.activiti.api.process.model.builders.ProcessPayloadBuilder;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.activiti.engine.RuntimeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Activiti7SpringbootDemoApplicationTests {

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    ProcessRuntime processRuntime;

    @Autowired
    SecurityUtil securityUtil;

    @Test
    void contextLoads() {
        securityUtil.logInAs("admin");
        String definedKey = "process22";
        ProcessInstance processInstance = processRuntime.start(ProcessPayloadBuilder.start().withProcessDefinitionKey(definedKey)
                .withVariable("user01","admin")
                .build());
        System.out.println(processInstance);
    }

}
