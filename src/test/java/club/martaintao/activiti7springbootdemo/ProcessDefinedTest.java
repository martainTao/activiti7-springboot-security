package club.martaintao.activiti7springbootdemo;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.engine.RepositoryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ProcessDefinedTest {

    @Autowired
    RepositoryService repositoryService;

    @Test
    public void getBPMNModel(){
        String definedId = "process:2:491942d7-56df-11eb-ac82-3c918040b17a";
        BpmnModel bpmnModel = repositoryService.getBpmnModel(definedId);
        System.out.println(bpmnModel);
    }
}
