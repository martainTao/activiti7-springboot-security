package club.martaintao.activiti7springbootdemo.service.mapper;

import java.sql.JDBCType;
import javax.annotation.Generated;
import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;

public final class UserDynamicSqlSupport {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.957+08:00", comments="Source Table: SYS_USER")
    public static final User user = new User();

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.957+08:00", comments="Source field: SYS_USER.id")
    public static final SqlColumn<Integer> id = user.id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.957+08:00", comments="Source field: SYS_USER.username")
    public static final SqlColumn<String> username = user.username;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.957+08:00", comments="Source field: SYS_USER.password")
    public static final SqlColumn<String> password = user.password;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.957+08:00", comments="Source field: SYS_USER.roles")
    public static final SqlColumn<String> roles = user.roles;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.957+08:00", comments="Source Table: SYS_USER")
    public static final class User extends SqlTable {
        public final SqlColumn<Integer> id = column("id", JDBCType.INTEGER);

        public final SqlColumn<String> username = column("username", JDBCType.VARCHAR);

        public final SqlColumn<String> password = column("password", JDBCType.VARCHAR);

        public final SqlColumn<String> roles = column("roles", JDBCType.VARCHAR);

        public User() {
            super("SYS_USER");
        }
    }
}