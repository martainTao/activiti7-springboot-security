package club.martaintao.activiti7springbootdemo.controller;

import club.martaintao.activiti7springbootdemo.common.BaseResponse;
import club.martaintao.activiti7springbootdemo.model.LocalUserDetail;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @GetMapping("getUserInfo")
    public BaseResponse getUserInfo(@AuthenticationPrincipal LocalUserDetail userDetail){
        if (userDetail==null){
            return BaseResponse.error("请重新登录");
        }
        return BaseResponse.success(userDetail);
    }
}
