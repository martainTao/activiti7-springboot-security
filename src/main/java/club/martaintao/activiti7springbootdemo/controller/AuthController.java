package club.martaintao.activiti7springbootdemo.controller;

import club.martaintao.activiti7springbootdemo.common.BaseResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {

    @PostMapping("/login")
    public BaseResponse requireAuth(String username,String password){
        /**
         * 这里不会执行，会调用successHandle。这里只是开放给swagger，方便登录而已
         */
        return BaseResponse.success();
    }
}
