package club.martaintao.activiti7springbootdemo.model.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.web.bind.annotation.RequestBody;

@ApiModel(value = "提交Bpmn xml 请求实体")
public class AddXMLRequest {
    @ApiModelProperty(value = "xml内容文本")
    String bpmnContent;
    @ApiModelProperty(value = "流程名称")
    String processName;

    public String getBpmnContent() {
        return bpmnContent;
    }

    public void setBpmnContent(String bpmnContent) {
        this.bpmnContent = bpmnContent;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }
}
