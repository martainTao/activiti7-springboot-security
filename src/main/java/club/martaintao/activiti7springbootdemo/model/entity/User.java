package club.martaintao.activiti7springbootdemo.model.entity;

import java.io.Serializable;
import javax.annotation.Generated;

public class User implements Serializable {
    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.948+08:00", comments="Source field: SYS_USER.id")
    private Integer id;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.951+08:00", comments="Source field: SYS_USER.username")
    private String username;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.952+08:00", comments="Source field: SYS_USER.password")
    private String password;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.952+08:00", comments="Source field: SYS_USER.roles")
    private String roles;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.952+08:00", comments="Source Table: SYS_USER")
    private static final long serialVersionUID = 1L;

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.95+08:00", comments="Source field: SYS_USER.id")
    public Integer getId() {
        return id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.951+08:00", comments="Source field: SYS_USER.id")
    public void setId(Integer id) {
        this.id = id;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.952+08:00", comments="Source field: SYS_USER.username")
    public String getUsername() {
        return username;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.952+08:00", comments="Source field: SYS_USER.username")
    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.952+08:00", comments="Source field: SYS_USER.password")
    public String getPassword() {
        return password;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.952+08:00", comments="Source field: SYS_USER.password")
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.952+08:00", comments="Source field: SYS_USER.roles")
    public String getRoles() {
        return roles;
    }

    @Generated(value="org.mybatis.generator.api.MyBatisGenerator", date="2021-01-12T15:15:53.952+08:00", comments="Source field: SYS_USER.roles")
    public void setRoles(String roles) {
        this.roles = roles == null ? null : roles.trim();
    }
}