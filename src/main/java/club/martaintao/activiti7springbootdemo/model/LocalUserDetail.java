package club.martaintao.activiti7springbootdemo.model;

import club.martaintao.activiti7springbootdemo.model.entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class LocalUserDetail implements UserDetails {

    private int id;
    private String username;
    private String password;
    private String roles;

    public static LocalUserDetail of(User user){
        LocalUserDetail localUserDetail = new LocalUserDetail();
        localUserDetail.id = user.getId();
        localUserDetail.username = user.getUsername();
        localUserDetail.password = user.getPassword();
        localUserDetail.roles = user.getRoles();
        return localUserDetail;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays.stream(roles.split(",")).map(e->new SimpleGrantedAuthority(e)).collect(Collectors.toSet());
    }


    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
