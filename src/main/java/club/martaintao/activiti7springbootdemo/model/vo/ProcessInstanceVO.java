package club.martaintao.activiti7springbootdemo.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.activiti.api.process.model.ProcessInstance;
import org.activiti.engine.repository.ProcessDefinition;

import java.io.Serializable;
import java.util.Date;

public class ProcessInstanceVO  implements Serializable {
    String id;
    String name;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    Date startDate;
    String initiator;
    String businessKey;
    String status;
    String processDefinitionId;
    String processDefinitionKey;
    String parentId;
    Integer processDefinitionVersion;
    ProcessDefinitionVO processDefinitionVO;

    public static ProcessInstanceVO of(ProcessInstance processInstance, ProcessDefinition processDefinition){
        ProcessInstanceVO instanceVO = new ProcessInstanceVO();
        instanceVO.setId(processInstance.getId());
        instanceVO.setName(processInstance.getName());
        instanceVO.setStartDate(processInstance.getStartDate());
        instanceVO.setInitiator(processInstance.getInitiator());
        instanceVO.setBusinessKey(processInstance.getBusinessKey());
        instanceVO.setStatus(String.valueOf(processInstance.getStatus()));
        instanceVO.setProcessDefinitionId(processInstance.getProcessDefinitionId());
        instanceVO.setProcessDefinitionKey(processInstance.getProcessDefinitionKey());
        instanceVO.setParentId(processInstance.getParentId());
        instanceVO.setProcessDefinitionVersion(processInstance.getProcessDefinitionVersion());
        if (processDefinition!=null){
            instanceVO.setProcessDefinitionVO(ProcessDefinitionVO.of(processDefinition));
        }
        return instanceVO;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getInitiator() {
        return initiator;
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public String getProcessDefinitionKey() {
        return processDefinitionKey;
    }

    public void setProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
    }


    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Integer getProcessDefinitionVersion() {
        return processDefinitionVersion;
    }

    public void setProcessDefinitionVersion(Integer processDefinitionVersion) {
        this.processDefinitionVersion = processDefinitionVersion;
    }

    public ProcessDefinitionVO getProcessDefinitionVO() {
        return processDefinitionVO;
    }

    public void setProcessDefinitionVO(ProcessDefinitionVO processDefinitionVO) {
        this.processDefinitionVO = processDefinitionVO;
    }
}
