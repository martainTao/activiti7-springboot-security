package club.martaintao.activiti7springbootdemo.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.activiti.api.task.model.Task;

import java.io.Serializable;
import java.util.Date;

public class TaskVO implements Serializable {
    
    String id;
    String owner;
    String assignee;
    String name;
    String description;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    Date createdDate;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    Date claimedDate;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    Date dueDate;
    int priority;
    String processDefinitionId;
    String processInstanceId;
    String parentTaskId;
    String status;
    String formKey;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    Date completedDate;
    Long duration;
    Integer processDefinitionVersion;
    String businessKey;
    boolean isStandalone;
    String instanceName;


    public static TaskVO of(Task task)
    {
        TaskVO taskVO = new TaskVO();
        taskVO.setId(task.getId());
        taskVO.setOwner(task.getOwner());
        taskVO.setAssignee(task.getAssignee());
        taskVO.setName(task.getName());
        taskVO.setDescription(task.getDescription());
        taskVO.setCreatedDate(task.getCreatedDate());
        taskVO.setClaimedDate(task.getClaimedDate());
        taskVO.setDueDate(task.getDueDate());
        taskVO.setPriority(task.getPriority());
        taskVO.setProcessDefinitionId(task.getProcessDefinitionId());
        taskVO.setProcessDefinitionVersion(task.getProcessDefinitionVersion());
        taskVO.setProcessInstanceId(task.getProcessInstanceId());
        taskVO.setParentTaskId(task.getParentTaskId());
        taskVO.setStatus(task.getStatus().toString());
        taskVO.setFormKey(task.getFormKey());
        taskVO.setCompletedDate(task.getCompletedDate());
        taskVO.setDuration(task.getDuration());
        taskVO.setBusinessKey(task.getBusinessKey());
        taskVO.setStandalone(task.isStandalone());
        return taskVO;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getClaimedDate() {
        return claimedDate;
    }

    public void setClaimedDate(Date claimedDate) {
        this.claimedDate = claimedDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public String getParentTaskId() {
        return parentTaskId;
    }

    public void setParentTaskId(String parentTaskId) {
        this.parentTaskId = parentTaskId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFormKey() {
        return formKey;
    }

    public void setFormKey(String formKey) {
        this.formKey = formKey;
    }

    public Date getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate(Date completedDate) {
        this.completedDate = completedDate;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Integer getProcessDefinitionVersion() {
        return processDefinitionVersion;
    }

    public void setProcessDefinitionVersion(Integer processDefinitionVersion) {
        this.processDefinitionVersion = processDefinitionVersion;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public boolean isStandalone() {
        return isStandalone;
    }

    public void setStandalone(boolean standalone) {
        isStandalone = standalone;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }
}
