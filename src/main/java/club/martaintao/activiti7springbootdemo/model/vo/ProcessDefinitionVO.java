package club.martaintao.activiti7springbootdemo.model.vo;


import org.activiti.engine.repository.ProcessDefinition;

import java.io.Serializable;

public class ProcessDefinitionVO implements Serializable {
    String id;
    String name;
    String key;
    int version;
    String deploymentId;
    String resourceName;
    String description;
    String diagramResourceName;
    Boolean isSuspended;

    public static ProcessDefinitionVO of(ProcessDefinition processDefinition){
        ProcessDefinitionVO definitionVO = new ProcessDefinitionVO();
        definitionVO.setId(processDefinition.getId());
        definitionVO.setName(processDefinition.getName());
        definitionVO.setKey(processDefinition.getKey());
        definitionVO.setVersion(processDefinition.getVersion());
        definitionVO.setDeploymentId(processDefinition.getDeploymentId());
        definitionVO.setResourceName(processDefinition.getResourceName());
        definitionVO.setDescription(processDefinition.getDescription());
        definitionVO.setDiagramResourceName(processDefinition.getDiagramResourceName());
        definitionVO.setSuspended(processDefinition.isSuspended());
        return definitionVO;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDiagramResourceName(String diagramResourceName) {
        this.diagramResourceName = diagramResourceName;
    }

    public void setSuspended(Boolean suspended) {
        isSuspended = suspended;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    public int getVersion() {
        return version;
    }

    public String getDeploymentId() {
        return deploymentId;
    }

    public String getResourceName() {
        return resourceName;
    }

    public String getDescription() {
        return description;
    }

    public String getDiagramResourceName() {
        return diagramResourceName;
    }

    public Boolean getSuspended() {
        return isSuspended;
    }
}
