package club.martaintao.activiti7springbootdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

@SpringBootApplication
public class Activiti7Application {

    public static void main(String[] args) {
        SpringApplication.run(Activiti7Application.class, args);
    }

}
